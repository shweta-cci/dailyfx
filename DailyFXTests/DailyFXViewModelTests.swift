//
//  DailyFXViewModelTests.swift
//  DailyFXTests
//
//  Created by Shweta Sawant on 23/01/22.
//

import XCTest
@testable import DailyFX

class DailyFXViewModelTests: XCTestCase {

    var viewModel:DailyFXViewModel!
    var daileyBriefings:[NewsItem]?
    
    override func setUpWithError() throws {
        try super.setUpWithError()
       viewModel = DailyFXViewModel()
        populateDataToTest()
    }
    
    func populateDataToTest() {
        let testBundle = Bundle(for: type(of: self))
        if let path = testBundle.path(forResource: "NewsData", ofType: ""){
            do {
                
                let data = try Data(contentsOf: URL(fileURLWithPath: path))
                let decodedData = try JSONDecoder().decode(NewsCategories.self,
                                                                      from: data)
                viewModel.categories = decodedData
              } catch {
                   // handle error
              }
        }
    }

    override func tearDownWithError() throws {
        viewModel = nil
        try super.tearDownWithError()
    }

    func testGetRowCountForSection() throws{
        XCTAssertEqual(viewModel.getRowCountForSection(0), 0) // Breaking news section count 0
        XCTAssertEqual(viewModel.getRowCountForSection(1), 1) 
    }
    
    func testGetNewsItemAtRow() throws {
        XCTAssertEqual(viewModel.getNewsItemAtRow(NSIndexPath(row: 0, section: 1) as IndexPath).url, viewModel.categories?.topNews![0].url)
    }
    
//    func testGetFormattedUpdatedDate() {
//        let todaysDate = Date()
//        XCTAssertEqual(viewModel.getFormattedUpdatedDate(0), "")
//        XCTAssertEqual(viewModel.getFormattedUpdatedDate(Double(todaysDate.timeIntervalSinceNow*1000)), "Today")
//    }
    
    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
