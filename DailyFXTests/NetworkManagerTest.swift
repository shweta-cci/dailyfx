//
//  NetworkManagerTest.swift
//  DailyFXTests
//
//  Created by Shweta Sawant on 23/01/22.
//

import XCTest
@testable import DailyFX

class NetworkManagerTest: XCTestCase {

    var networkManager:NetworkManager?
    
    override func setUpWithError() throws {
        try super.setUpWithError()
        networkManager = NetworkManager()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testNetworkCall() {
    
        let promise = expectation(description: "Status code: 200")
        
        networkManager?.execute("https://content.dailyfx.com/api/v1/dashboard", completion: { data, success in
            
            if success {
                promise.fulfill()
            }else{
                XCTFail("Failed")
            }
        })
        
        wait(for: [promise], timeout: 10)

    }
    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
