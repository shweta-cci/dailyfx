//
//  NetworkManager.swift
//  DailyFX
//
//  Created by Shweta Sawant on 19/01/22.
//

import UIKit

class NetworkManager: NSObject {
    
    typealias NetwrokRequestCompletionHandler = (_ data: Data?, _ success: Bool) -> Void
    
    func execute(_ url: String, completion:@escaping NetwrokRequestCompletionHandler){
        
        guard let requestURl = URL(string: url) else {
            return
        }
        
        let urlRequest: URLRequest = URLRequest(url: requestURl)
        let task = URLSession.shared.dataTask(with: urlRequest) {
            (data, response, error) -> Void in            
            completion(data ?? nil,true)
        }
        task.resume()
    }
}
