//
//  DataManager.swift
//  DailyFX
//
//  Created by Shweta Sawant on 19/01/22.
//

import UIKit

class DataManager: NSObject {

    typealias CategoriesCompletionHandler = (_ categories: NewsCategories, _ success: Bool) -> Void
    
    
    func getNewsItems(_ url: String, completion: @escaping CategoriesCompletionHandler) {
        NetworkManager().execute(url) { data, success in
            if let jsonData = data {
                do{
                     let decodedData = try JSONDecoder().decode(NewsCategories.self,
                                                                           from: jsonData)
                    completion(decodedData, success)
                    
                }
                catch {
                }
            }
            
        }
    }
}
