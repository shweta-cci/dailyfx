//
//  NewsCategories.swift
//  DailyFX
//
//  Created by Shweta Sawant on 18/01/22.
//

import Foundation

struct NewsCategories: Codable {
    let breakingNews: Array<NewsItem>?
    let topNews: Array<NewsItem>?
    let dailyBriefings: DailyBriefing?
    let technicalAnalysis: Array<NewsItem>?
    let specialReport: Array<NewsItem>?
    
}
