//
//  Author.swift
//  DailyFX
//
//  Created by Shweta Sawant on 18/01/22.
//

import Foundation

struct Author: Codable {
    
    let name:String?
    let title:String?
    let bio:String?
    let email:String?
    let phone:String?
    let facebook:String?
    let twitter:String?
    let googleplus:String?
    let subscription:String?
    let rss:String?
    let descriptionLong:String?
    let descriptionShort:String?
    let photo:String?
}
