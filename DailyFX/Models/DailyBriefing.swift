//
//  DailyBriefing.swift
//  DailyFX
//
//  Created by Shweta Sawant on 18/01/22.
//

import Foundation

struct DailyBriefing:Codable {
    let eu:Array<NewsItem>?
    let asia:Array<NewsItem>?
    let us:Array<NewsItem>?
}
