//
//  NewsItem.swift
//  DailyFX
//
//  Created by Shweta Sawant on 18/01/22.
//

import Foundation


struct NewsItem: Codable {
    
    let title:String?
    let url:String?
    let description:String?
    let content:String?
    let firstImageUrl:String?
    let headlineImageUrl:String?
    let articleImageUrl:String?
    let backgroundImageUrl:String?
    let videoType:String?
    let videoId:String?
    let videoUrl:String?
    let videoThumbnail:String?
    let newsKeywords:String?
    let authors:Array<Author>?
    let instruments:Array<String>?
    let tags:Array<String>?
    let categories:Array<String>?
    let displayTimestamp:Double?
    let lastUpdatedTimestamp:Double?
}
