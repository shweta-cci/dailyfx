//
//  DaileyFXCategories.swift
//  DailyFX
//
//  Created by Shweta Sawant on 18/01/22.
//

import UIKit

protocol DailyFXNewsItemsDelegate {
    func newsItemSelected(_ newsItem: NewsItem?)
}

class DailyFXNewsItems: UITableViewController, UISplitViewControllerDelegate {

    var newsItemDelegate: DailyFXNewsItemsDelegate?
    var dailyFXViewModel = DailyFXViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.estimatedRowHeight = 70
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = nil
        tableView.register(UINib(nibName: "HeaderView", bundle: nil), forHeaderFooterViewReuseIdentifier: "HeaderView")
        
        self.tableView.reloadData()
        
                
    }

    @IBAction func reloadNews(_ sender: Any) {
        dailyFXViewModel.fetchNews()
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 5
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        dailyFXViewModel.getRowCountForSection(section)
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewsItemCell", for: indexPath) as! NewsItemCell
        let newsItem:NewsItem = dailyFXViewModel.getNewsItemAtRow(indexPath)
        cell.loadNewItem(newsItem, updatedDate: dailyFXViewModel.getFormattedUpdatedDate(newsItem.lastUpdatedTimestamp ?? 0))
        cell.layoutIfNeeded()
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedNewsItem = dailyFXViewModel.getNewsItemAtRow(indexPath)
        newsItemDelegate?.newsItemSelected(selectedNewsItem)
        
        if let detailViewController = newsItemDelegate as? NewsDetailsViewController {
          splitViewController?.showDetailViewController(detailViewController, sender: nil)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = tableView.dequeueReusableHeaderFooterView(withIdentifier: "HeaderView") as! HeaderView
        headerView.setTitle(dailyFXViewModel.categoriesHeader[section])
        
        return headerView
    }

    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return CGFloat(dailyFXViewModel.getSectionHeaderHeight(section))
    }
}

extension DailyFXNewsItems: DailyFXViewModelDelegate {
    func dataFetched() {
        DispatchQueue.main.async { [self] in
            tableView.reloadData()
            if let firstVisibleIndex = tableView.indexPathsForVisibleRows?[0]{
                newsItemDelegate?.newsItemSelected(dailyFXViewModel.getNewsItemAtRow(firstVisibleIndex))

            }
        }
    }
}

class HeaderView: UITableViewHeaderFooterView {
    
    @IBOutlet weak var headerTitle: UILabel!
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }

    func setTitle(_ title: String) {
        headerTitle.text = title
        self.tintColor = UIColor.clear
    }
}
