//
//  NewsDetailsViewController.swift
//  DailyFX
//
//  Created by Shweta Sawant on 21/01/22.
//

import UIKit
import WebKit

class NewsDetailsViewController: UIViewController {
    @IBOutlet weak var newsDetailsWebview: WKWebView!

    var selectedNewsItem: NewsItem?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadDetails()
    }

    func loadDetails() {
        
        if let newsURL =  URL(string: selectedNewsItem?.url ?? ""){
            newsDetailsWebview.load(URLRequest(url: newsURL))
        }
    }

}

extension NewsDetailsViewController : DailyFXNewsItemsDelegate {

    func newsItemSelected(_ newsItem: NewsItem?) {
        selectedNewsItem = newsItem
        loadDetails()
    }
}
