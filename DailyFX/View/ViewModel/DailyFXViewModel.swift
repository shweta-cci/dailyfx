//
//  DailyFXViewModel.swift
//  DailyFX
//
//  Created by Shweta Sawant on 20/01/22.
//

import Foundation

protocol DailyFXViewModelDelegate: AnyObject {
    func dataFetched()
}
enum TableSection: Int {
    case BreakingNews = 0
    case TopNews
    case DailyBreifing
    case TechnicalAnalysis
    case SpecialReport
}

class DailyFXViewModel {
    
    let headerHeight = 90
    var categories:NewsCategories?
    
    let headerTitles = ["Breaking News", "Top News", "Daily Briefing", "Technical Analysis", "Special Report"]
    var categoriesHeader = [String](repeating: "", count: 5)
    
    var dailyBriefings:[NewsItem]?
    
    weak var delegate: DailyFXViewModelDelegate?
    
    init() {
        fetchNews()
    }
    
    func fetchNews() {
        categoriesHeader = [String](repeating: "", count: 5)
        DataManager().getNewsItems("https://content.dailyfx.com/api/v1/dashboard") { categoriesFetched, didFetch in
            self.categories = categoriesFetched
            self.dailyBriefings = {
                var combinedArray: Array<NewsItem> = self.categories?.dailyBriefings?.asia ?? []
                combinedArray.append(contentsOf: self.categories?.dailyBriefings?.eu ?? [])
                combinedArray.append(contentsOf: self.categories?.dailyBriefings?.us ?? [])
                return combinedArray
            }()
            self.delegate?.dataFetched()
        }
    }
    
    func getRowCountForSection(_ section:Int) -> Int {
        
        var rowCount = 0
        
        switch section {
            case TableSection.BreakingNews.rawValue:
                rowCount = categories?.breakingNews?.count ?? 0
            case TableSection.TopNews.rawValue:
                rowCount = categories?.topNews?.count ?? 0
            case TableSection.DailyBreifing.rawValue:
                rowCount = dailyBriefings?.count ?? 0
            case TableSection.TechnicalAnalysis.rawValue:
                rowCount = categories?.technicalAnalysis?.count ?? 0
            case TableSection.SpecialReport.rawValue:
                rowCount = categories?.specialReport?.count ?? 0
            default:
                rowCount = 0
        }
        
        if rowCount > 0 {
            categoriesHeader.insert(headerTitles[section], at: section)
        }
        return rowCount
    }
    
    func getNewsItemAtRow(_ index: IndexPath) -> NewsItem{
        
        switch index.section {
            case TableSection.BreakingNews.rawValue:
                return (categories?.breakingNews?[index.row])!
            case TableSection.TopNews.rawValue:
                return (categories?.topNews?[index.row])!
            case TableSection.DailyBreifing.rawValue:
                return (dailyBriefings?[index.row])!
            case TableSection.TechnicalAnalysis.rawValue:
                return (categories?.technicalAnalysis?[index.row])!
            case TableSection.SpecialReport.rawValue:
                return (categories?.specialReport?[index.row])!
            default:
                return (categories?.breakingNews?[index.row])!
        }
    }
    
    func getSectionHeaderHeight(_ section: Int) -> Int {
        switch section {
            case TableSection.BreakingNews.rawValue:
                return categories?.breakingNews?.count ?? 0 > 0 ? headerHeight:0
            case TableSection.TopNews.rawValue:
                return categories?.topNews?.count ?? 0 > 0 ? headerHeight:0
            case TableSection.DailyBreifing.rawValue:
                return dailyBriefings?.count ?? 0 > 0 ? headerHeight:0
            case TableSection.TechnicalAnalysis.rawValue:
                return categories?.technicalAnalysis?.count ?? 0 > 0 ? headerHeight:0
            case TableSection.SpecialReport.rawValue:
                return categories?.specialReport?.count ?? 0 > 0 ? headerHeight:0
            default:
                return categories?.breakingNews?.count ?? 0 > 0 ? headerHeight:0
        }
    }
    
    func getFormattedUpdatedDate(_ date: Double) -> String {
        
        if date == 0 {
            return ""
        }
        
        let formatter = DateComponentsFormatter()
        formatter.unitsStyle = .full
        
        let dateFromDouble = Date(timeIntervalSince1970: TimeInterval(date)/1000)
        let now = Date()
        
        let calendar = NSCalendar.current
        let components1: Set<Calendar.Component> = [.year, .month, .weekOfMonth, .day]
        let components = calendar.dateComponents(components1, from: dateFromDouble, to: now)

        if components.year ?? 0 > 0 {
            formatter.allowedUnits = .year
        } else if components.month ?? 0 > 0 {
            formatter.allowedUnits = .month
        } else if components.weekOfMonth ?? 0 > 0 {
            formatter.allowedUnits = .weekOfMonth
        } else if components.day ?? 0 > 0 {
            formatter.allowedUnits = .day
        }
        
        if calendar.isDateInToday(dateFromDouble){
            return "Today"
        }else if calendar.isDateInYesterday(dateFromDouble){
            return "Yesterday"
        }
        
        guard let dateString = formatter.string(for: components) else {
            return ""
        }
        
        return dateString.appending(" ago")
    }
    
}
