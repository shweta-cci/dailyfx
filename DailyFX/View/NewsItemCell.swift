//
//  NewsItemCell.swift
//  DailyFX
//
//  Created by Shweta Sawant on 19/01/22.
//

import UIKit

class NewsItemCell: UITableViewCell {

    @IBOutlet weak var updatedAt: UILabel!
    @IBOutlet weak var newsDetails: UILabel!
    @IBOutlet weak var authorImage: UIImageView!
    @IBOutlet weak var newsHeading: UILabel!
    @IBOutlet weak var authorName: UILabel!
    @IBOutlet weak var newsPreviewImage: UIImageView!
    @IBOutlet weak var authorIcon: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func loadNewItem(_ item: NewsItem, updatedDate: String) {
        newsHeading.text = item.title
        authorName.text = item.authors?[0].name
        newsDetails.text = item.description
        
        if let imageUrl = URL(string: item.headlineImageUrl ?? "") {
            newsPreviewImage.load(url: imageUrl)
        }
        
        if let authorImage = URL(string: item.authors?[0].photo ?? "") {
            authorIcon.load(url: authorImage)
        }
        updatedAt.text = "Updated: \(updatedDate)"
    }

}

extension UIImageView {
    func load(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
